<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

class ATranslator {
	protected $tr_array;

	public function __construct($context = 'core', $lang = null) {
		$this->tr_array = array();
		$this->initDictionary($context,$lang);
	}


	public function initDictionary( $context = 'core', $lang = null, $dictionary_path = null) {
		if ( $lang==null || $lang=='' || (!is_string($lang) )) {
			$lang = Encore::getOption('localize');
		}
		if ( $dictionary_path == null || (!is_string($dictionary_path))) {
			$dictionary_path = Encore::getOption('localize_path');
		}

		$path = $dictionary_path . $context . DS . $lang . '.json';
		$tr_json = file_get_contents($path);

		if( FALSE==$tr_json){
			throw new AException("Can't open localize file: " . $path, 0 );
		}

		if(!isset($this->tr_array[$context])) {
			$this->tr_array[$context] = array();
		}


		$this->tr_array[$context][$lang] = (array)Encore::fromJson($tr_json);
	}

	public function getDictionary($context = 'core', $lang = null) {
		if ( $lang==null || $lang=='' || (!is_string($lang) )) {
			$lang = Encore::getOption('localize');
		}
		return (isset($this->tr_array[$context])) ? ( (isset($this->tr_array[$context][$lang])) ? $this->tr_array[$context][$lang] : null) : null ;
	}

	public function getContexts() {
		return array_keys($this->tr_array);
	}

	public function getContextLanguages($context) {
		return (isset($this->tr_array[$context])) ? array_keys($this->tr_array[$context]) : null;
	}

	public function tr($key, $context='core', $lang = null) {
		if ( $lang==null || $lang=='' || (!is_string($lang) )) {
			$lang = Encore::getOption('localize');
		}
		$ret = $key;
		if( isset($this->tr_array[$context]) && isset($this->tr_array[$context][$lang]) && isset($this->tr_array[$context][$lang][$key])) {
			$ret = $this->tr_array[$context][$lang][$key];
		}
		return $ret;
	}



}
