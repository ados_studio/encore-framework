<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

class AException extends Exception{

	const CONTEXT_RADIUS  = 5;

	public function __construct($message, $errorLevel = 0, $errorFile = '', $errorLine = 0) {
		parent::__construct($message, $errorLevel);
		$this->file = $errorFile;
		$this->line = $errorLine;
	}

    private function getFileContext()
    {
    	$file = $this->getFile();
    	if($file == '')return;
    	$line_number = $this->getLine();
        $context = array();
        $i = 0;
        foreach(file($file) as $line) {
            $i++;
            if($i >= $line_number - self::CONTEXT_RADIUS && $i <= $line_number + self::CONTEXT_RADIUS) {
                if ($i == $line_number) {
                    $context[] = ' >>'. $i ."\t". htmlspecialchars($line);
                } else {
					$context[] = '   '. $i ."\t". htmlspecialchars($line);
                }
            }
            if($i > $line_number + self::CONTEXT_RADIUS) break;
        }
        return "\n". implode("", $context);
    }

	public function report(){
		$message = nl2br($this->getMessage());
		$result = <<<HTML
  <script>
  function TextDump() {
    w = window.open('', "Error text dump", "scrollbars=yes,resizable=yes,status=yes,width=1000px,height=800px,top=100px,left=100px");
    w.document.write('<html><body>');
    w.document.write('<h1>' + document.getElementById('Title').innerHTML + '</h1>');
    w.document.write(document.getElementById('Context').innerHTML);
    w.document.write(document.getElementById('Trace').innerHTML);
    w.document.write(document.getElementById('Request').innerHTML);
    w.document.write('</body></html>');
    w.document.close();
  }
  </script>
	<div style="width:99%; position:relative">
	<h3 id='Title'>ENCORE EXCEPTION: {$message}</h2>
	<a href="#" onclick="TextDump(); return false;">Raw dump</a>
	<div id="Context" style="display: block;"><h3>Error with code {$this->getCode()} in '{$this->getFile()}' around line {$this->getLine()}:</h3><pre>{$this->getFileContext()}</pre></div>
	<div id="Trace"><h2>Call stack</h2><pre>{$this->getTraceAsString()}</pre></div>
	<div id="Request"><h2>Request</h2><pre>
HTML;
        $result .= var_export($_REQUEST, true);
        $result .= "</pre></div></div>";
        return $result;
	}


}

set_error_handler(create_function('$c, $m, $f, $l', 'throw new AException($m, $c, $f, $l);'), E_ALL);