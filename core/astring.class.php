<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

class AString {

	private $text;

	function __construct ($arg=null) {
		$this->text = $this->converter($arg);
	}

	/**
	 *
	 * Appends the string $arg onto the end of this string.
	 * @param AString $arg
	 */
	public function append($arg) {
		return $this->from($this->text . $this->converter($arg));
	}

	public function fill($ch, $ln = 0) {
		$ret = AString::from('');
		if($ln==0) $ln = $this->length();
		for($i=0;$i<$ln;$i++) {
			$ret = $ret->append($ch);
		}
		return $ret;
	}

	/**
	 *
	 * Prepends the string $arg onto the end of this string.
	 * @param AString $arg
	 */
	public function prepend($arg) {
		return $this->from($this->converter($arg) . $this->text);
	}


	/**
	 *
	 * Returns a copy of this string with the lowest numbered place marker replaced by string a, i.e., %1, %2, ..., %99.
	 * @param AString $replace
	 */
	public function arg ($replace){
		$ret = new AString($this->text . '_');
		$num_tpl = new AString('0123456789');
		$rp = $this->converter($replace);
		$maxi = $this->length()-1;
		$ar = array();

		for($i=0; $i<$maxi; $i++) {
			if ($ret->at($i)=='%') {
				$numstr = AString::from($ret)->mid($i+1, 2);
				if($num_tpl->contains($numstr->at(0))) {
					if (!$num_tpl->contains($numstr->at(1))) {
						$numstr = $numstr->at(0);
					}
					$ar[(int)(string)$numstr] = $i;
				}
			}
		}

		ksort($ar);

		foreach ($ar as $key=>$item) {
			$ret = $ret->replace('%'.$key, $rp, $item-1);
			$rp = '%'.$key;
		}

		$ret = $ret->mid(0,$ret->length()-1);
		return $this->from($ret);
	}


	/**
	 *
	 * Returns the character at the given index position in the string. The position must be a valid index position in the string
	 * @param $position
	 */
	public function at($position) {
		return AString::from($this->text)->mid($position, 1);
	}

	/**
	 *
	 * Clears the contents of the string and makes it empty.
	 */
	public function clear() {
		$this->text = null;
		return $this;
	}

	/**
	 *
	 * Lexically compares this string with the other string and returns an integer less than, equal to, or greater than zero if this string is less than, equal to, or greater than the other string.
	 * @param AString $other
	 */
	public function compare ($other, $sens=false) {
		$ret = 0;
		if($sens){
			$ret = strcmp($this->toUpper(), AString::from($other)->toUpper());
		} else {
			$ret = strcmp($this->text, $other);
		}
		return $ret;
	}

	/**
	 *
	 * Enter description here ...
	 * @param AString $search
	 * @param AString $offset
	 * @param boolean $sens
	 */
	public function contains ($search, $offset = 0, $sens = false) {
		return ($this->indexOf($search, $offset, $sens) != null);
	}

	/**
	 *
	 * Returns the index position of the first occurrence of the string $search in this string, searching forward from index position $offset. Returns FALSE if $search is not found.
	 * @param AString $search
	 * @param integer $offset
	 * @param bool $sens
	 */
	public function indexOf ($search, $offset = 0, $sens=false ) {
		$ret = null;
		if(defined('MB_OVERLOAD_STRING')){
			$ret = ($sens) ? mb_stripos($this->text, (string)$search, $offset): mb_strpos((string)$this->text, (string)$search, $offset) ;
		} else {
			$ret = ($sens) ? stripos($this->text, (string)$search, $offset): strpos((string)$this->text, (string)$search, $offset) ;
		}
		if($ret === false) $ret = null;
		return $ret;
	}

	/**
	 *
	 * Inserts the string str at the given index position and returns a reference to this string.
	 * @param int $position
	 * @param AString $str
	 */
	public function insert ($position, $str) {
		$len = $this->length();
		if ($position<0 || $position > $len) return null;
		$left = AString::from($this->text)->left($position);
		$right = AString::from($this->text)->right($len-$position);
		return $this->from($left . $str . $right);
	}

	public static function join ($mix, $spliter) {
		return self::from(implode($mix,$spliter));
	}

	/**
	 *
	 * Returns TRUE if the string has no characters; otherwise returns FALSE.
	 */
	public function isEmpty() {
		return ($this->text == null || $this->text == '');
	}

	/**
	 *
	 * Returns TRUE if this string is NULL; otherwise returns FALSE.
	 */
	public function isNull() {
		return ($this->text == null);
	}

	/**
	 *
	 * Returns the index position of the last occurrence of the string str in this string, searching backward from index position from. If from is -1 (default), the search starts at the last character; if from is -2, at the next to last character and so on. Returns -1 if str is not found.
	 * @param AString $search
	 * @param integer $offset
	 * @param bool $sens
	 */
	public function lastIndexOf($search, $offset = 0, $sens = false) {
		$ret = null;
		if(defined('MB_OVERLOAD_STRING')){
			$ret = ($sens) ? mb_strripos($this->text, (string)$search, $offset): mb_strrpos($this->text, (string)$search, $offset) ;
		} else {
			$ret = ($sens) ? strripos($this->text, (string)$search, $offset): strrpos($this->text, (string)$search, $offset) ;
		}
		return $ret;
	}

	/**
	 *
	 * Enter description here ...
	 * @param int $len
	 */
	public function left($len) {
		return  $this->mid(0,$len);
	}

	/**
	 *
	 * Returns the number of characters in this string. Equivalent to size()
	 */
	public function length () {
		$len = null;
		if(defined('MB_OVERLOAD_STRING')) {
			$len = mb_strlen($this->text);
		} else {
			$len = strlen($this->text);
		}
		return $len;
	}

	/**
	 *
	 * Returns a string that contains n characters of this string, starting at the specified position index.
	 * @param integer $position
	 * @param integer $size
	 */
	public function mid ($position, $size) {
		$ret = null;
		if(defined('MB_OVERLOAD_STRING')) {
			$ret = mb_substr($this->text, $position, $size);
		} else {
			$ret = substr($this->text, $position, $size);
		}
		if($ret===false) {
			$ret = null;
		}
		return AString::from($ret);
	}

	public function replace ($search, $repl, $offset = 0) {
		return $this->from(str_replace($search, $repl, $this->text, $offset));
	}

	/**
	 *
	 * Enter description here ...
	 * @param int $len
	 */
	public function right($len) {
		return $this->mid($len*(-1), $len);
	}

	public function set($val) {
		$this->text = $this->converter($val);
		return $this;
	}

	public function simplified() {
		$res = $this->replace("\t",' ')->replace("\s",' ')->replace("\n",' ')->replace("\x0B",' ')->replace("\0",' ')->replace("\r",' ')->split(' ');
		$ret = array();
		foreach($res as $word) {
		    if($word != '' || $word !=null) {
				$ret[] = $word;
		    }
		}
		return $this->join($ret,' ');
	}

	public function split($separator) {
		return explode($separator, $this->text);
	}

	/**
	 *
	 * Returns a lowercase copy of the string.
	 * @param AString $str
	 */
	public function toLower() {
		$ret = null;
		$val = $this->text;

		if(defined('MB_OVERLOAD_STRING')) {
			$ret = mb_strtolower($val);
		} else {
			$ret = strtolower($val);
		}

		return $this->from($ret);
	}

	/**
	 *
	 * Returns an uppercase copy of the string.
	 * @param AString $str
	 */
	public function toUpper() {
		$ret = null;
		$val = $this->text;

		if(defined('MB_OVERLOAD_STRING')) {
			$ret = mb_strtoupper($val);
		} else {
			$ret = strtoupper($val);
		}

		return $this->from($ret);
	}


	public function toString () {
		return $this->text;
	}

	public function trim($charlist="\s\n\r\t\0\x0B ", $l=true, $r=true) {
		return self::trimmed($charlist, $l, $r);
	}

	public function trimmed($charlist="\s\n\r\t\0\x0B ", $l=true, $r=true) {
		$ret = AString::from($this->text);
		if(defined('MB_OVERLOAD_STRING')) {
			$charlist = AString::from($charlist);
			$len = $this->length();
			if($l) {
				for($i = 0; $i< $len; $i++) {
					if(! $charlist->contains($this->at($i)) ) {
						break;
					}
				}
			}
			$ret = $ret->right($len-$i);

			$len = $ret->length();
			if($r) {
				for($i = $len -1; $i>=0; $i--) {
					if(! $charlist->contains($ret->at($i)) ) {
						break;
					}
				}
			}
			$ret = $ret->left($i+1);
		} else {
			if($l && $r) {
				$ret = trim($this->text,$charlist);
			} elseif ($l && !$r ) {
				$ret = ltrim($this->text,$charlist);
			} else {
				$ret = rtrim($this->text,$charlist);
			}

		}
		return $ret;
	}

	public function urlencode() {
		$ret = $this->text;
		if(defined('MB_OVERLOAD_STRING')) {
			$ret = urlencode(utf8_encode($ret));
		} else {
			$ret = urlencode($ret);
		}
		return AString::from($ret);
	}

	/**
	 *
	 * Create new AString object from $arg
	 * @param AString compitable $arg
	 * @return AString
	 */
	public static function from ($arg) {
		return new AString($arg);
	}

	public function __toString() {
		return (string)$this->text;
	}

	protected static function converter($arg) {
		$text = null;
		if (is_string($arg)) {
			$text = $arg;
		}
		if ( (is_object($arg) && (is_subclass_of($arg, 'AString') || get_class($arg)=='AString')) || is_float($arg) || is_int($arg) ) {
			$text = (string)$arg;
		}
		return $text;
	}
}

?>
