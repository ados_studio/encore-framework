<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

require_once (ENC_CORE_PATH.'aexception.class.php');
require_once (ENC_CORE_PATH.'astring.class.php');
require_once (ENC_CORE_PATH.'aobject.class.php');
require_once (ENC_CORE_PATH.'translator.class.php');
require_once (ENC_CORE_PATH.'module.abstract.class.php');
require_once (ENC_CORE_PATH.'cache.abstract.class.php');
require_once (ENC_LIBS_PATH.'JSON.php');

require_once (ENC_CORE_PATH.'validator.class.php');

class Encore {
	protected function __construct() {}
	protected function __clone() {}

	/******** PROPERTIES ********/
	protected static $options = array( "CORE" => array() );
	protected static $modules = array();
	protected static $session_key = null;
	protected static $user_imprint = null;

	/********* SIGNALS **********/
	/**
	 * Набір сигналів для підєднання функцій-слотів
	 */
	protected static $signals = array(
		'beforeInit'           => array(),
		'afterInit'            => array(),
		'beforeRegisterModule' => array(),
		'afterRegisterModule'  => array(),
		'beforeInitModule'     => array(),
		'afterInitModule'      => array(),
		'beforeLog'            => array(),
		'afterLog'             => array(),
		'beforeSessionStart'   => array(),
		'afterSessionStart'    => array(),
		'sessionLogout'        => array(),
		'sessionLogin'         => array()
	);

	/********* SERVICES *********/
	protected static $json_service = null;
	protected static $localize = null;
	protected static $cache_manager = null;

	/********** METHODS *********/

	// AOBJECT API

	/**
	 * Підключити слот-функцію до сигналу ядра
	 *
	 * @param string $signal
	 * @param callback $funct
	 */
	public static function connect($signal, $funct){
		if(isset(self::$signals[$signal])&&(!in_array($funct,self::$signals[$signal]))){
			array_push(self::$signals[$signal],$funct);
		}
	}

	/**
	 * Відключити слот-функцію від сигналу ядра
	 *
	 * @param string $signal
	 * @param callback $funct
	 */
	public static function disconnect($signal,$funct){
		if(isset(self::$signals[$signal])&&(in_array($funct,self::$signals[$signal]))){
			unset(self::$signals[$signal][array_search($funct,self::$signals[$signal])]);
		}
	}

	/**
	 * Випустити сигнал
	 *
	 * @param string $signal
	 * @param array $arg
	 */
 	protected static function emit($signal,$arg=NULL){
		if(isset(self::$signals[$signal])&&is_array(self::$signals[$signal])){
			foreach(self::$signals[$signal] as $funct){
				call_user_func_array($funct,array($arg));
			}
		}
	}
	// LOG API

	/**
	 * Занести в журнал системне повыдомлення
	 *
	 * @param string $section
	 * @param string $owner
	 * @param string $message
	 * @param integer $level
	 */
	public static function log($section, $owner, $message, $level ) {
		$accept = true;
		self::emit('beforeLog', array(
			'section' => &$section,
			'owner'   => &$owner,
			'message' => &$message,
			'level'   => &$level,
			'accept'  => &$accept
		));
		if ( $accept && $level <= self::getOption('log_level',0) && self::existsOption('log_file')) {
			$fp = fopen(self::getOption('log_file'), 'a');
			if (flock($fp, LOCK_EX)) {
				$dd = date(self::getOption('datetime_format','d.m.Y H:s:s'));
  				fwrite($fp, ">>>$dd > $level:$section - $owner\n $message \n <<< \n");
    			flock($fp, LOCK_UN);
			}
			fclose($fp);
		}
		self::emit('afterLog', array(
			'section' => &$section,
			'owner'   => &$owner,
			'message' => &$message,
			'level'   => &$level
		));
	}

	// OPTIONS API
	/**
	 * Перевырити наявність системної опції
	 *
	 * @param string $name
	 * @param string $section
	 */
	public static function existsOption ($name, $section='CORE') {
		$usect = AString::from($section)->toUpper()->toString();
		return (isset(self::$options[$usect])&&isset(self::$options[$usect][AString::from($name)->toUpper()->toString()]));
	}

	/**
	 * Отримати значення системної опції, або дефолтне значення
	 *
	 * @param string $name
	 * @param mixed $default
	 * @param string $section
	 */
	public static function getOption ($name, $default=null, $section='CORE') {
		$ret = $default;
		$uname = AString::from($name)->toUpper()->toString();
		$usect = AString::from($section)->toUpper()->toString();
		if (isset(self::$options[$usect])&&isset(self::$options[$usect][$uname])) {
			$ret = self::$options[$usect][$uname];
		}
		return $ret;
	}

	/**
	 * Встановити системну опцію
	 *
	 * @param string $name
	 * @param mixed $value
	 * @param string $section
	 */
	public static function setOption ($name, $value, $section='CORE') {
		$usect = AString::from($section)->toUpper()->toString();
		if(!isset(self::$options[$usect])) {
			self::$options[$usect] = array();
		}
		self::$options[$usect][AString::from($name)->toUpper()->toString()] = $value;
	}

	// LOCALIZE API

	/**
	 * додати словник локалізації
	 *
	 * @param string $dictionary_path
	 */
	public static function initDictionary ($context, $lang = null) {
		if(self::$localize == null) {
			self::$localize = new ATranslator();
		}
		self::$localize->initDictionary($context, $lang);
	}

	/**
	 * Отримати зведений словник у вигляді індексованого масива
	 *
	 */
	public static function getDictionary($context, $lang = null){
		if(self::$localize == null) {
			self::$localize = new ATranslator();
		}
		return self::$localize->getDictionary($context, $lang);
	}

	/**
	 * локалізувати стрічку
	 *
	 * @param string $val
	 */
	public static function tr($val, $context = 'core', $lang = null) {
		if(self::$localize == null) {
			self::$localize = new ATranslator();
		}
		return self::$localize->tr($val, $context, $lang);
	}

	/**
	 * Отримати ініціалізовані контексти словника
	 *
	 */
	public static function getTranslationContexts() {
		if(self::$localize == null) {
			self::$localize = new ATranslator();
		}
		return self::$localize->getContexts();
	}

	/**
	 * Отримати ініціалізовані мови для контексту словника
	 *
	 * @param unknown_type $context
	 */
	public static function getTranslationContextLangs($context) {
		if(self::$localize == null) {
			self::$localize = new ATranslator();
		}
		return self::$localize->getContextLanguages($context);
	}


	// CACHE API

	/**
	 * Отримати менеджер кешування
	 *
	 */
	public static function getCacheManager() {
		if(self::$cache_manager == null) {
			self::$cache_manager = self::initCacheManager();
		}
		return self::$cache_manager;
	}

	/**
	 * Отримати значення з кешу
	 *
	 * @param string $key
	 * @param array $options
	 */
	public static function cacheGet($key, $options = array()) {
		if(self::$cache_manager == null) {
			self::$cache_manager = self::initCacheManager();
		}
		return self::$cache_manager->get($key,$options);
	}

	/**
	 * Записати значення в кеш
	 *
	 * @param string $key
	 * @param mixed $var
	 * @param array $options
	 */
	public static function cacheSet($key, $var, $options = array()) {
		if(self::$cache_manager == null) {
			self::$cache_manager = self::initCacheManager();
		}
		return self::$cache_manager->set($key,$var,$options);
	}

	/**
	 * Видалити значення з кешу
	 *
	 * @param string $key
	 * @param options $options
	 */
	public static function cacheDelete($key, $options = array()) {
		if(self::$cache_manager == null) {
			self::$cache_manager = self::initCacheManager();
		}
		return self::$cache_manager->delete($key,$options);
	}

	/**
	 * Отримати поточний час актуальності значення в кеші
	 *
	 */
	public static function getCacheExpire() {
		if(self::$cache_manager == null) {
			self::$cache_manager = self::initCacheManager();
		}
		return self::$cache_manager->getExpire();
	}

	/**
	 * Задати час актуальності значення в кеші
	 *
	 * @param integer $seconds
	 */
	public static function setCacheExpire($seconds) {
		if(self::$cache_manager == null) {
			self::$cache_manager = self::initCacheManager();
		}
		return self::$cache_manager->setExpire($seconds);
	}

	/**
	 * Ініціалізувати об`єкт для управління кешуванням
	 *
	 */
	protected static function initCacheManager() {
		$cache_classname = self::getOption('cache_class', 'FileCache');
		include(dirname(__FILE__).DS.'cache'.DS.AString::from($cache_classname)->toLower().'.class.php');
		return new $cache_classname;
	}

	// SESSION API

	/**
	 * Почати сесію, якщо користувач авторизується. (Авторизація реалізується модулем)
	 *
	 */
	public static function sessionStart() {

		$session_cookie_key = self::getOption('session_cookie_key','session_key');  //ключ ідентифікатора сесії в куках
		$auth_flag = true;															//флаг який передається модулю авторизації
		$cm = self::getCacheManager();
		$expire = self::getOption('session_expire',3600);							// таймайт сесії
		if(isset($_COOKIE[$session_cookie_key])) {									
			$skey = $_COOKIE[$session_cookie_key];			
			$cm->setExpire($expire);
			$session_imprint = $cm->get('session.key.'.$skey.'.imprint');
			$cm->defExpire();

			if( $session_imprint == null || count(array_diff_assoc(self::getClientImprint(), $session_imprint)) > 0 ) {					
				// якщо змінився відпечаток клієнта закриваєм сесію і кидаєм попередження в лог
				setcookie($session_cookie_key, '', (time()+1),'/');
				self::log('startSession', 'ENCORE', AString::from(Encore::tr('WR_SESSION_DIFF_IMPRINT'))->arg($_SERVER['REMOTE_ADDR']), 1);
			} else {				
				if(isset($_POST['logout'])) {		
					// якщо клієнт просить закрити сесію			
					self::cacheDelete('session.key.'.$skey.'.imprint');
					setcookie($session_cookie_key, '', (time()+1),'/');
					self::emit('sessionLogout', array('session_key' => $skey));
				} else {		
					// кастуєм сигнал з передачою синоніма флага 			
					self::emit('beforeSessionStart', array('flag' => &$auth_flag));
					// якщо жоден з модулів не заблокував сесію, тоді продовжуємо її
					if($auth_flag) {						
						self::$session_key = $skey;			
						// поновляємо куку			
						setcookie($session_cookie_key, self::$session_key, (time()+$expire),'/');
						// поновляємо відпечаток в кеші
						$cm->setExpire($expire);
						$cm->set('session.key.'.$skey.'.imprint', $session_imprint);
						$cm->defExpire();
						self::emit('afterSessionStart');
					}
				}
			}
		} elseif(isset($_POST['login'])) {
			$skey = self::generateUniqueKey(42);
			self::emit('sessionLogin', array('flag' => &$auth_flag, 'session_key' => &$skey));
			if ($auth_flag) {
				self::$session_key = $skey;
				$cm->setExpire($expire);
				$cm->set('session.key.'.self::$session_key.'.imprint',self::getClientImprint());
				$cm->defExpire();
				setcookie($session_cookie_key, self::$session_key, (time()+$expire),'/');
				self::emit('afterSessionStart');
			}
		}
	}

	/**
	 * Перевірити чи почата сесія
	 *
	 */
	public static function sessionStarted() {
		return (self::$session_key != null);
	}

	/**
	 * Запамятати значення в сесії
	 *
	 * @param string $key
	 * @param mixed $var
	 */
	public static function sessionSet($key, $var) {
		if(self::$session_key != null) {
			$expire = self::getOption('session_expire',3600);
			$cm = self::getCacheManager();
			$cm->setExpire($expire);
			$cm->set(self::$session_key.'value'.$key, $var);
			$cm->defExpire();
		}
	}

	/**
	 * Отримати значення в сесії
	 *
	 * @param string $key
	 */
	public static function sessionGet($key) {
		$ret = null;
		if(self::$session_key != null) {
			$expire = self::getOption('session_expire',3600);
			$cm = self::getCacheManager();
			$cm->setExpire($expire);
			$ret = $cm->get(self::$session_key.'value'.$key);
			$cm->defExpire();
		}
		return $ret;
	}

	/**
	 * Отримати ключ поточної сесії
	 *
	 */
	public static function sessionKey() {
		return self::$session_key;
	}

	/**
	 * Отримати відпечаток клієнта
	 *
	 */
	protected static function getClientImprint() {
		if(self::$user_imprint == null) {
			self::$user_imprint = array(
				'ip'         => $_SERVER['REMOTE_ADDR'],
				'language'   => $_SERVER['HTTP_ACCEPT_LANGUAGE'],
				'user_agent' => $_SERVER['HTTP_USER_AGENT']
			);
		}
		return self::$user_imprint;
	}

	// JSON API
	/**
	 * Конвертувати значення в формат JSON
	 *
	 * @param mixed $value
	 */
	public static function toJson ($value) {
		if(self::$json_service == null ) {
			self::$json_service = new Services_JSON();
		}
		return self::$json_service->encode($value);
	}

	/**
	 * Конвертувати JSON в масив
	 *
	 * @param string $value
	 */
	public static function fromJson ($value) {
		if(self::$json_service == null ) {
			self::$json_service = new Services_JSON();
		}
		return self::$json_service->decode($value);
	}

	// OTHER API

	/**
	 * Згнерувати випадкову стрічку
	 *
	 * @param integer $lenght
	 * @param string $chars
	 */
	public static function generateUniqueKey($length, $chars = 'ABCDEFGHJKLMNOPQRSTUVWXYZ1234567890') {
		$chars_length = (strlen($chars) - 1);
    	$string = $chars{rand(0, $chars_length)};
	    for ($i = 1; $i < $length; $i = strlen($string))  {
        	$r = $chars{rand(0, $chars_length)};
	        if ($r != $string{$i - 1}) $string .=  $r;
		}
   		return $string;
	}

	/**
	 * Перевірити наявність ключів в масиві
	 *
	 * @param array $arg
	 * @param array $keys
	 */
	public static function isSetKeys(array $arg, array $keys) {
		$ret = true;
		foreach( $keys as $key )  {
			$ret = $ret && isset($arg[$key]);
			if(!$ret) break;
		}
		return $ret;
	}

	/**
	 * Згенерувати зписок опцій для html тега select
	 *
	 * @param $list масив значень
	 * @param $key_field поле значень
	 * @param $title_field поле відображення назв
	 * @param $active_key наперед вибрана опція
	 */
	public static function makeSelectList(array $list, $key_field, $title_field, $active_keys = null ) {
		$ret = '';
		$active = '';
		foreach ($list as $item) {
			$active = ($item[$key_field] == $active_keys ||  (is_array($active_keys) && in_array($item[$key_field],$active_keys) ) ) ? ' selected=""' : '';
			$ret .=	'<option value="'.$item[$key_field].'"'.$active.'>'.$item[$title_field].'</option>';
		}
		return $ret;
	}

	// MSERVER API

	/**
	 * Зареєструвати в системний модуль
	 *
	 * @param string $name
	 * @param string $class
	 * @param string $path
	 */
	public static function registerModule ($name, $class, $path, $autoload = false ) {
		try {
			self::emit('beforeRegisterModule', array('name' => &$name, 'class' => &$class, 'path' => &$path));
			if(isset(self::$modules[$name])) return;
			self::$modules[$name] = array(
				"name"  => $name,
				"class" => $class,
				"path"  => $path
			);
			if ($autoload) {
				$md = self::getModule($name);
				$md->init();
			}
			self::emit('afterRegisterModule', array('name' => &$name, 'class' => &$class, 'path' => &$path));
		} catch (AException $e) {
			self::log('ENCORE', 'INIT METHOD', $e->report(), 0);
			exit(1);
		}
	}

	/**
	 * Отримати об'єкт модуля
	 *
	 * @param string $name
	 */
	public static function getModule ($name) {
		$ret = null ;
		if (isset(self::$modules[$name])) {
			if(isset(self::$modules[$name]['object'])) {
				$ret = &self::$modules[$name]['object'];
			} else {
				$ret = self::initModule($name);
			}
			//$ret = (isset(self::$modules[$name]['object'])) ? self::$modules[$name]['object'] : self::initModule($name);
		}
		return $ret;
	}

	/**
	 * Перевірити залежність модулів
	 *
	 * @param string $owner
	 * @param array,string $names
	 * @throws AException
	 */
	public static function requireModules ($owner, $names) {
		$anames = array();
		if(is_string($names)) {
			$anames = AString::from($names)->replace(' ', '')->split(',');
		} elseif(is_array($names)) {
			$anames = array();
		} else {
			throw new AException(AString::from(Encore::tr('ER_FAILURE_ATTRIBUTE_TYPE'))->arg('Encore::requireModules()')->arg('names') );
		}
		try {
			foreach($anames as $name) {
				if(!isset(self::$modules[$name])) {
					throw new AException(AString::from(Encore::tr('ER_REQUIRE_MODULE'))->arg($owner)->arg($name));
				}
			}
		} catch (AException $e) {
			self::log('ENCORE', 'INIT METHOD', $e->report(), 0);
			exit(1);
		}
	}

	/**
	 * Перевірити наявність модуля
	 *
	 * @param string $name
	 */
	public static function moduleExists($name) {
		return (isset(self::$modules[$name]));
	}
	/**
	 * Створити об'єкт модуля
	 *
	 * @param string $name
	 */
	protected static function initModule($name){
		$ret = null;
		
		try {
			if(isset(self::$modules[$name])) {
				self::emit('beforeInitModule',array( 'name' => $name));
				if (file_exists(self::$modules[$name]['path'])) {
					include_once(self::$modules[$name]['path']);
					self::$modules[$name]['object'] = new self::$modules[$name]['class'];
					$ret = &self::$modules[$name]['object'];
					self::emit('afterInitModule',array(
						'name'   => self::$modules[$name]['name'],
						'class'  => self::$modules[$name]['class'],
						'path'   => self::$modules[$name]['path'],
						'object' => &self::$modules[$name]['object']
						));
				} else {
					Encore::log('ENCORE','INIT MODULE METHOD', AString::from(self::tr('ER_ENCORE_INIT_MODULE'))->arg($name), 2);
				}
			}
		} catch (AException $e) {
			self::log('ENCORE', 'INIT METHOD', $e->report(), 0);
			exit(1);
		}

		return $ret;
	}

	//************ INIT ************//

	/**
	 * Розпочати ініціацію
	 *
	 */
	public static function init() {
		try {
			$ret = self::getModule('init');
			if($ret != null) {
				$ret->init();
			}
			return $ret;
		} catch (AException $e) {
			self::log('ENCORE', 'INIT METHOD', $e->report(), 0);
			exit(1);
		}
	}
	
	//************ DEVELOP ************//
	
	public static function vardump($var) {
		echo('<br />');
		var_dump($var);
		echo('<br />');
	}
}