<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

class FileCache extends EncoreCache {

	protected $cache_dir = '';

	public function __construct() {
		parent::__construct();
		$this->initialized = true;
		$this->cache_dir = Encore::getOption('cache_path', null);
		if( $this->cache_dir == null ) {
			throw new AException(AString::from(Encore::tr("Undefined core option '%1'"))->arg('cache_path'));
		}
	}


	/**
	 * (non-PHPdoc)
	 * @see EncoreCache::set()
	 */
	public function set ( $key, $var, $options = array() ) {
		if(isset($options['serialize_type']) && $options['serialize_type'] == 'JSON') {
			$str_val = Encore::toJson($var);
		} else {
			$str_val = serialize($var);
		}
		$file_name = $this->pathCache($key) . md5($key);
		$f = fopen($file_name, 'w+');
		if (flock($f, LOCK_EX)) {
			fwrite($f, $str_val);
			flock($f, LOCK_UN);
		}
		fclose($f);
	}

	/**
	 * (non-PHPdoc)
	 * @see EncoreCache::get()
	 */
	public function get( $key,  $options = array() ) {
		$ret = true;
 		$data = '';
 		$file_name = $this->getPathCache($key) . md5($key);
		if (!file_exists($file_name)) {
			$ret = false;
		} else {
			if ( ((filemtime($file_name) + $this->expire) < time()) || (!$data = file($file_name)) ) {
				$ret = false;
			}
		}
		if($ret) {
			$ret = unserialize(implode('', $data));
		} else {
			$ret = null;
		}
		return $ret;
	}

	/**
	 * (non-PHPdoc)
	 * @see EncoreCache::delete()
	 */
	public function delete ( $key,  $options = array() ) {
		$file_name = $this->getPathCache($key) . md5($key);
		if(file_exists($file_name)) {
			unlink($file_name);
		}
	}

	/**
	 * Отримати шлях для ключа ( відсутні директорії створюються )
	 *
	 * @param string $value_name
	 */
	private function pathCache($value_name) {
		$md5 = md5($value_name);
		$first_literals = array($md5{0}, $md5{1}, $md5{2}, $md5{3});
		$path = '';
		foreach ($first_literals as $dir) {
			$path .= $dir.DS;

			if (!file_exists($this->cache_dir . $path)) {
				if (!mkdir($this->cache_dir . $path, 0775)) return false;
			}
		}
		return $this->cache_dir . $path;
	}

	/**
	 * Отримати дерикторії шляху для ключа
	 *
	 * @param string $value_name
	 */
	private function getPathCache($value_name) {
		$md5 = md5($value_name);
		$first_literals = array($md5{0}, $md5{1}, $md5{2}, $md5{3});
		return $this->cache_dir.implode(DS, $first_literals).DS;
	}
}