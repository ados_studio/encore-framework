<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

class Validator {
	public static function is_email($email) {
		return preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $email);
	}

	public static function is_latin($string) {
		return preg_match('|^[a-z]{0,}$|is', $string);
	}

	public static function is_login($string) {
		return preg_match('|^[a-z]{1,1}[a-z0-9\.\+\-\_\*]{3,}$|is', $string);
	}

	public static function is_phone($phone) {
		return preg_match('|^[-\+0-9() ]+$|is', $phone);
	}
}