<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

class ADate {

	protected $dt;

	function __construct($d = null, $m = null, $y = null) {
		$this->dt = mktime(0,0,0,$m,$d,$y);
	}

	public static function from ($d=0,$m=0,$y=0) {
		return new ADate($d,$m,$y);
	}

	public function addDays($d) {
		$thdate = getdate($this->dt);
		//$thdate.addDays($d
		return new ADate();
	}

	public function addMonths ($m) {

	}

	public function addYears ($y) {

	}

	public function day () {
		return (integer)$this->dt->format('j');

	}

	public function dayOfWeek () {
		return (integer)$this->dt->format('j');
	}

	public function dayOfYear () {
		return (integer)$this->dt->format('z');
	}

	public function daysInMonth () {
		return (integer)$this->dt->format('t');
	}

	public function daysInYear () {
		return ((integer)$this->dt->format('L')==1)?366:365;
	}

	public function daysTo ( $adate ) {

	}

	public function getDate ( $y=0, $m=0, $d=0 ) {

	}

	public function isNull () {

	}

	public function isValid ($y=0,$m=0,$d=0) {

	}

	public function month () {

	}

	public function setDate ( $y=0,$m=0,$d=0 ) {

	}

	public function toJulianDay () {

	}

	public function toString ( $format = '' ) {

	}

	public function weekNumber ( $yearNumber = 0 ) {

	}

	public function year () {

	}

	// STATIC
	public function currentDate () {

	}

	public function fromJulianDay ( $jd ) {

	}

	public function fromString ( $str, $format = '' ) {

	}

	public function isLeapYear ( $y ) {

	}

	public function longDayName ( $weekday, $type = 0 ) {

	}

	public function longMonthName ( $m, $type = 0 ) {

	}

	public function shortDayName ( $weekday, $type = 0 ) {

	}

	public function shortMonthName ( $m, $type = 0 ) {

	}

	/*
	bool	operator!= ( const QDate & d ) const
	bool 	operator< ( const QDate & d ) const
	bool 	operator<= ( const QDate & d ) const
	bool 	operator== ( const QDate & d ) const
	bool 	operator> ( const QDate & d ) const
	bool 	operator>= ( const QDate & d ) const
	*/
}