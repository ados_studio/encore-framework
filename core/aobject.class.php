<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

 function connect(AObject $base_object,$signal,$callback_object,$slot){
 		$base_object->connect($signal,array($callback_object,$slot));
 }

 abstract class AObject {
	private $signals;

	function __construct(){
		$signals = array();
	}

	public function connect($signal,$funct){
		if(isset($this->signals[$signal])&&(!in_array($funct,$this->signals[$signal]))){
			array_push($this->signals[$signal],$funct);
		}
	}

	public function disconnect($signal,$funct){
		if(isset($this->signals[$signal])&&(in_array($funct,$this->signals[$signal]))){
			unset($this->signals[$signal][array_search($funct,$this->signals[$signal])]);
		}
	}

 	protected function emit($signal,$arg=NULL){
		if(isset($this->signals[$signal])&&is_array($this->signals[$signal])){
			foreach($this->signals[$signal] as $funct){
				call_user_func_array($funct,array($arg));
			}
		}
	}

	protected function registerSignals($signals_array){
		if(is_array($signals_array)){
			$this->signals = array();
			foreach ($signals_array as $signal_name){
				$this->signals[$signal_name] = array();
			}
		}
	}
 }

?>
