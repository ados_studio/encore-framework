<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

abstract class EncoreCache {

	protected $options = array();
	protected $prefix = '';
	protected $initialized = false;
	protected $expire = 0;
	protected $lastKey = null;

	public function __construct($options = array()) {
		$this->options = $options;
		$this->prefix = Encore::getOption('cache_prefix','default');
		$this->expires = Encore::getOption('cache_expire',300);
	}

	public function isInitialized()	{
		return (boolean) $this->initialized;
	}

	public function getRealKey ( $key ) {
		return $this->prefix.$key;
	}

	public function getLastKey () {
		return $this->lastKey;
	}

	public function setExpire ($val) {
		$this->expire = (integer)$val;
	}

	public function getExpire () {
		return $this->expire;
	}

	public function defExpire () {
		$this->expire = Encore::getOption('cache_expire',300);
	}

	abstract public function set ( $key, $var, $options = array() );
	abstract public function delete ( $key,  $options = array() );
	abstract public function get( $key,  $options = array() );

}
