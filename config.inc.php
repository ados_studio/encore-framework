<?php

/**
 *
 * This file is part of Encore Libs
 *
 * Copyright (c) 2011, Andriy Chvyl
 *
 * Contact:  achvyl@gmail.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0
 * as published by the Free Software Foundation and appearing in the file LICENSE
 * included in the packaging of this file. Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be met:
 * http://www.gnu.org/copyleft/gpl.html.
 *
 */

$php_os = (strtoupper(substr(PHP_OS,0,3))==='WIN');
if(!defined('PATH_SEPARATOR')) define('PATH_SEPARATOR',($php_os)?';':':');
if(!defined('DIRECTORY_SEPARATOR')) define('DIRECTORY_SEPARATOR',($php_os)?'\\':'/');
if(!defined('DIR_SEPARATOR')) define('DIR_SEPARATOR',DIRECTORY_SEPARATOR);
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
unset ($php_os);

if(!defined('ENC_CORE_PATH')) define('ENC_CORE_PATH', dirname(__FILE__).DS.'core'.DS);
if(!defined('ENC_LIBS_PATH')) define('ENC_LIBS_PATH', dirname(__FILE__).DS.'libs'.DS);

require_once (ENC_CORE_PATH.'encore.class.php');



// ******* CORE OPTIONS *******

Encore::setOption('log_level', 0); // 0 - Error, 1 - Warning, 2 - Debug
Encore::setOption('log_file', dirname(__FILE__).DS.'logs'.DS.'log.txt');

Encore::setOption('localize_path', dirname(__FILE__).DS.'localize'.DS);
Encore::setOption('localize_system','ua');
Encore::setOption('localize','ua');
Encore::setOption('date_format','d.m.Y');

Encore::setOption('cache_path',dirname(__FILE__).DS.'cache'.DS);
